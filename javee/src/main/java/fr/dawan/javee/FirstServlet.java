package fr.dawan.javee;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
	    System.out.println("Méthode init");
	    
	    // Paramètre d’initialisation <init-param>
        System.out.println("Paramètre d’initialisation (<init-param>)  annee= " + config.getInitParameter("annee"));

        // paramètres d’initialisation de l’application <context-param>
        System.out.println("Context param (<context-param>)  nom =" + config.getServletContext().getInitParameter("nom"));

		super.init(config);
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
	    System.out.println("Méthode destroy");
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    System.out.println("Méthode service");
	    super.service(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    System.out.println("Méthode doGet");
	    
	    // Paramètre d’initialisation <init-param>
	    System.out.println("Paramètre d’initialisation (<init-param>)  annee= "+ getServletConfig().getInitParameter("annee"));
	    
	    // paramètres d’initialisation de l’application <context-param>
        System.out.println("Context param (<context-param>)  nom =" + getServletContext().getInitParameter("nom"));

        // écriture dans la réponse
        response.getWriter().println("<html>");
        response.getWriter().println("<head>");
        response.getWriter().println("<title>Hello World!</title>");
        response.getWriter().println("</head>");
        response.getWriter().println("<body>");
        response.getWriter().println("<p>Hello World!</p>");
        response.getWriter().println("</body>");
        response.getWriter().println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    System.out.println("Méthode doPost");
		doGet(request, response);
	}

}
