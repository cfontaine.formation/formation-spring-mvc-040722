package fr.dawan.springcore.composants;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository("formationRepository")
@Primary
@Scope("prototype")
public class FormationRepository {

    private String urlBdd;

    public FormationRepository() {

    }

    public FormationRepository(String urlBdd) {
        this.urlBdd = urlBdd;
    }

    public String getUrlBdd() {
        return urlBdd;
    }

    public void setUrlBdd(String urlBdd) {
        this.urlBdd = urlBdd;
    }

    @PostConstruct
    public void init() {
        System.out.println("Méthode initiation");
        if(urlBdd==null) {
            urlBdd="urlDefault";
        }
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destruction");
    }
    
    @Override
    public String toString() {
        return "FormationRepository [urlBdd=" + urlBdd + ", hashCode()=" + hashCode() + "]";
    }

}
