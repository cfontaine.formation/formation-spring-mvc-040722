package fr.dawan.springcore.composants;

import org.springframework.stereotype.Repository;

@Repository
public class LivreRepository {
    
    private String urlDatasource;   
    
    public LivreRepository() {
    }

    public LivreRepository(String urlDatasource) {
        this.urlDatasource = urlDatasource;
    }

    public String getUrlDatasource() {
        return urlDatasource;
    }

    public void setUrlDatasource(String urlDatasource) {
        this.urlDatasource = urlDatasource;
    }

    @Override
    public String toString() {
        return "LivreRepository [urlDatasource=" + urlDatasource + "]";
    }
    
    

}
