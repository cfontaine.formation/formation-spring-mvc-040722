package fr.dawan.springcore.composants;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Lazy
@Service
//@Named
public class FormationService {
    @Resource
   @Autowired
  // @Qualifier(value = "repositoryFormation2")
//    @Inject
//    @Named(value = "repositoryFormation2")
    private FormationRepository repository;

    public FormationService() {
        System.out.println("Constructeur par défaut FormationService");
    }

    @Autowired(required = false)
    public FormationService(/*@Qualifier(value ="formationRepository")*/FormationRepository repository) {
        System.out.println("Constructeur 1 paramètres FormationService");
        this.repository = repository;
    }

    public FormationRepository getRepository() {
        return repository;
    }

   // @Autowired
    public void setRepository(/*@Qualifier(value ="formationRepository")*/FormationRepository repository) {
       System.out.println("setRepository");
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "FormationService [repository=" + repository + ", hashCode()=" + hashCode() + "]";
    }

}
