package fr.dawan.springcore.composants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LivreService {

    @Autowired
    private LivreRepository repository;

    public LivreService() {

    }

    public LivreService(LivreRepository repository) {
        super();
        this.repository = repository;
    }

    public LivreRepository getRepository() {
        return repository;
    }

    public void setRepository(LivreRepository repository) {
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "LivreService [repository=" + repository + "]";
    }
    
}
