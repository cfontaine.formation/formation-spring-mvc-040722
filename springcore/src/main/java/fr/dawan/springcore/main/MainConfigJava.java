package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.composants.FormationRepository;

public class MainConfigJava {

    public static void main(String[] args) {
       ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConf.class);
       
       Adresse adr1=ctx.getBean("adresse1", Adresse.class);
       System.out.println(adr1);
       
       FormationRepository fr1=ctx.getBean("formationRepository",FormationRepository.class);
       System.out.println(fr1);
       
       Contact c1=ctx.getBean("contact1",Contact.class);
       System.out.println(c1);
       
       Contact c2=ctx.getBean("contact2",Contact.class);
       System.out.println(c2);

    }

}
