package fr.dawan.springcore.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;

@Configuration
@ComponentScan(basePackages = "fr.dawan.springcore")
public class AppConf {
    
    @Bean
    public Adresse adresse1() {
        return new Adresse();
    }
    
    @Bean
    public Adresse adresse2() {
        return new Adresse("1 rue Esquermoise","Lille","59000");
    }
    
    @Bean(name="contact1")
    public Contact contactA(Adresse adresse2) { // ref en xml
        return new Contact("John","Doe",adresse2); 
    }
    
    @Bean
    public Contact contact2() {
        return new Contact("Jane","Doe",adresse1()); 
    }
    
    @Bean(initMethod = "initialisation", destroyMethod = "destruction")
    public Formation formation1() {
        return new Formation("Java","java init",21,900.0);
    }
    

}
