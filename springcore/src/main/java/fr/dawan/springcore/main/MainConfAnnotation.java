package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.composants.FormationRepository;
import fr.dawan.springcore.composants.FormationService;

public class MainConfAnnotation {

    public static void main(String[] args) {
       ApplicationContext ctx=new ClassPathXmlApplicationContext("confAnnotation.xml");
       System.out.println("-------------------------------");
       FormationRepository fr1=ctx.getBean("formationRepository",FormationRepository.class);
       System.out.println(fr1);
       
       FormationRepository fr1B=ctx.getBean("formationRepository",FormationRepository.class);
       System.out.println(fr1B);
       
       FormationService fs1=ctx.getBean("formationService",FormationService.class);
       System.out.println(fs1); 
       System.out.println("-------------------------------");
       ((AbstractApplicationContext)ctx).close();
    }

}
