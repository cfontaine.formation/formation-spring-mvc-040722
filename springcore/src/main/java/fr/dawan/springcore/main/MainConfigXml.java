package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formateur;
import fr.dawan.springcore.beans.Formation;

public class MainConfigXml {

    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] { "beans.xml", "adresses.xml" });
        System.out.println("----------------------------------------");
        
        // getBean permet de récupérer les instances des beans depuis le conteneur
        Formation f1 = ctx.getBean("formation1", Formation.class);
        System.out.println(f1);

        Formation f2 = ctx.getBean("word", Formation.class);
        System.out.println(f2);

        Formation f3 = ctx.getBean("formation3", Formation.class);
        System.out.println(f3);
        
        // Porté (Scope)

        //- Singleton:
        // Pour un singleton (ex: formation 1), getBean("formation1") retourne toujours
        // la même instance
        Formation f1B = ctx.getBean("formation1", Formation.class);
        System.out.println(f1B);
        
        //- Prototype:
        // Pour un prototype (ex: formation3) getBean("formation3") retourne une nouvelle instance à chaque appel
        // correspond à un new
        Formation f3B = ctx.getBean("formation3", Formation.class);
        System.out.println(f3B);

        // Lazy-init
        Formation f4 = ctx.getBean("formation4", Formation.class);
        System.out.println(f4);
        
        
        // Exercice Création de Bean
        Adresse adr1 = ctx.getBean("adresse1", Adresse.class);
        System.out.println(adr1);

       Adresse adr2=ctx.getBean("adresse2",Adresse.class);
       System.out.println(adr2);


       Contact c1=ctx.getBean("contact1",Contact.class);
       System.out.println(c1);
       Contact c2=ctx.getBean("contact2",Contact.class);
       System.out.println(c2);
       Contact c3=ctx.getBean("contact3",Contact.class);
       System.out.println(c3);
       
//       Contact c4=ctx.getBean("contact4",Contact.class);
//       System.out.println(c4);
       
//       Contact c5=ctx.getBean("contact5",Contact.class);
//       System.out.println(c5);

//        Contact c6 = ctx.getBean("contact6", Contact.class);
//        System.out.println(c6);
        
       Formation f5 = ctx.getBean("formation5", Formation.class);
       System.out.println(f5);
       
       
        Client cl1=ctx.getBean("client1",Client.class);
        System.out.println(cl1);
        
        Client cl2=ctx.getBean("client2",Client.class);
        System.out.println(cl2);
        
//        Contact c8 = ctx.getBean("contact8", Contact.class);
//        System.out.println(c8);
        
        Client cl3=ctx.getBean("client3",Client.class);
         System.out.println(cl3);  
         
        Formateur fo1=ctx.getBean("formateur1",Formateur.class);
        System.out.println(fo1);
         
        Contact c9=ctx.getBean("contact9",Contact.class);
        System.out.println(c9);
        
        Client cl4=ctx.getBean("client4",Client.class);
        System.out.println(cl4);
        System.out.println("---------------------------------------");
       ((AbstractApplicationContext) ctx).close();
    }

}
