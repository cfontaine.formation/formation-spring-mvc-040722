package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Livre;

public class MainConfigXmlExo2 {

    public static void main(String[] args) {
        ApplicationContext ctx=new ClassPathXmlApplicationContext("livres.xml");
        Livre lvr1=ctx.getBean("l1",Livre.class);
        System.out.println(lvr1);
        
        Livre lvr2=ctx.getBean("l2",Livre.class);
        System.out.println(lvr2);
        
        Livre lvr3=ctx.getBean("l3",Livre.class);
        System.out.println(lvr3);
    }

}
