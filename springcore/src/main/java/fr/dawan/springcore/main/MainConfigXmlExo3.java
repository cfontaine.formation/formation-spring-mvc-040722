package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.composants.LivreRepository;
import fr.dawan.springcore.composants.LivreService;

public class MainConfigXmlExo3 {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("livres.xml");

        LivreRepository lr = ctx.getBean("livreRepository", LivreRepository.class);
        System.out.println(lr);

        LivreService ls = ctx.getBean("livreService", LivreService.class);
        System.out.println(ls);
    }

}
