package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Formateur implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String prenom;
    
    private String nom;
    
    private Adresse adresse;

    private String centre;

    public Formateur() {
        super();
    }

    public Formateur(String prenom, String nom, Adresse adresse, String centre) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.centre = centre;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }

    @Override
    public String toString() {
        return "Formateur [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", centre=" + centre + "]";
    }

}
