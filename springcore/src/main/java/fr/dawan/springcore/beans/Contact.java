package fr.dawan.springcore.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;
    
    private String nom;
    
    private Adresse adresse;

    private List<String> telephone=new ArrayList<>();
    
    public Contact() {

    }

    public Contact(String prenom, String nom, Adresse adresse) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<String> getTelephone() {
        return telephone;
    }

    public void setTelephone(List<String> telephone) {
        this.telephone = telephone;
    }

    
    @Override
    public String toString() {
        String tmp="";
        for(String t :telephone) {
            tmp+=t +" "; 
        }
        return "Contact [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse +  " Téléphone ["+tmp+ "]"+", hashCode()=" + hashCode()
                + "]";
    }
    
}
