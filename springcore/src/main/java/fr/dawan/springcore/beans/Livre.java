package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Livre implements Serializable {

    private static final long serialVersionUID = 1L;

    private String titre;
    
    private Contact auteur;
    
    private int annee;

    public Livre() {
    }

    public Livre(String titre, Contact auteur, int annee) {
        this.titre = titre;
        this.auteur = auteur;
        this.annee = annee;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Contact getAuteur() {
        return auteur;
    }

    public void setAuteur(Contact auteur) {
        this.auteur = auteur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public String toString() {
        return "Livre [titre=" + titre + ", auteur=" + auteur + ", annee=" + annee + "]";
    } 
    
}
