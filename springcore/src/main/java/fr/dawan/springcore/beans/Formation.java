package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nom;

    private String description;

    private int duree;

    private double prix;

    public Formation() {
        System.out.println("Constructeur par défaut formation");
    }

    public Formation(String nom, String description, int duree, double prix) {
        System.out.println("Constructeur 4 paramètres formation");
        this.nom = nom;
        this.description = description;
        this.duree = duree;
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        System.out.println("Setter nom Formation");
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Formation [nom=" + nom + ", description=" + description + ", duree=" + duree + ", prix=" + prix
                + ", hashCode()=" + hashCode() + "]";
    }
    
    public void initialisation() {
        System.out.println("Méthode initialisation");
    }

    public void destruction() {
        System.out.println("Méthode destruction");
    }
}
