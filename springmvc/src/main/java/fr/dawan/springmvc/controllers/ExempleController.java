package fr.dawan.springmvc.controllers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Provider;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springmvc.beans.Article;
import fr.dawan.springmvc.beans.Personne;
import fr.dawan.springmvc.dao.ArticleDao;

@Controller
@RequestMapping("/exemple")
@SessionAttributes("user2")
public class ExempleController {

    private static int cpt;

    @Autowired
    private Provider<Personne> provider;

    @Autowired
    ArticleDao dao;
    // FakeArticleDao dao;

    @Autowired
    JavaMailSender emailSender;

    @RequestMapping("")
    public String exemple() {
        return "exemple";
    }

    // RequestMapping
    @RequestMapping(value = { "/hello", "/helloworld" }, method = RequestMethod.GET)
    // ou @GetMapping({"/hello","/helloworld"})
    public String helloWorld() {
        return "helloworld"; // une méthode du controleur retourne le nom de la vue
    }

    // Exemple jstl
    @GetMapping("/testjstl")
    public String testJstl(Model model) {
        model.addAttribute("msg", "Test jstl");
        model.addAttribute("v1", 1);
        List<String> lst = Arrays.asList("TV 4K", "Smartphone", "cable HDMI", "Souris Gaming");
        model.addAttribute("produits", lst);
        return "exemplejstl";
    }

    // Model
    @GetMapping("/testmodel")
    public String testModel(Model model) {
        model.addAttribute("msg", "Test model");
        return "exemple";
    }

    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        ModelAndView mdv = new ModelAndView();
        mdv.addObject("msg", "Test ModelAndView");
        mdv.setViewName("exemple");
        return mdv;
    }

    @GetMapping(value = "/testparams", params = "id=12")
    public String testParams(Model model) {
        model.addAttribute("msg", "La requête contient un paramètre id égal à 12");
        return "exemple";
    }

    // @PathVariable
    @GetMapping("/testpath/{id}")
    public String testPathVariable(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id = " + id);
        return "exemple";
    }

    @GetMapping("/testpathmulti/{id}/action/{action}")
    public String testPathVariable(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id = " + id + "et action=" + action);
        return "exemple";
    }

    @GetMapping("/testpathamb/{id:[0-9]+}")
    public String testPathVariableNum(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id = " + id);
        return "exemple";
    }

    @GetMapping("/testpathamb/{name:[a-zA-Z]+}")
    public String testPathPb2(@PathVariable String name, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre name = " + name);
        return "exemple";
    }

    @GetMapping({ "/testpathoption/{id}", "/testpathoption" })
    public String testPathVariableOptionnel(@PathVariable(required = false) String id, Model model) {
        model.addAttribute("msg", "@Pathvariable=" + id);
        return "exemple";
    }

    @GetMapping("/testparam")
    public String testRequestParam(@RequestParam String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping("/testparammulti")
    public String testRequestParamMulti(@RequestParam String id, @RequestParam String nom, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id + " nom=" + nom);
        return "exemple";
    }

    @GetMapping(value = "/testparamamb", params = "id")
    public String testRequestParamAmbId(@RequestParam String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping(value = "/testparamamb", params = "nom")
    public String testRequestParamAmbNom(@RequestParam String nom, Model model) {
        model.addAttribute("msg", "@RequestParam nom=" + nom);
        return "exemple";
    }

    @GetMapping("/testparamdefault")
    public String testRequestParamDefault(@RequestParam(defaultValue = "0") String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testRequestParamConV(@RequestParam int id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + (id * 2));
        return "exemple";
    }

    @GetMapping("/testconvdate")
    public String testConvDate(@DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam LocalDate date, Model model) {
        model.addAttribute("msg", date.format(DateTimeFormatter.ofPattern("dd MMMM  yy")));
        return "exemple";
    }

    // @RequesHeader
    @GetMapping("/testheader")
    public String testHeader(@RequestHeader("user-agent") String useragent, Model model) {
        model.addAttribute("msg", useragent);
        return "exemple";
    }

    @GetMapping("/testallheader")
    public String testAllHeaders(@RequestHeader HttpHeaders headers, Model model) {
        List<String> headersParams = new ArrayList<>();
        Set<Entry<String, List<String>>> sent = headers.entrySet();
        for (Entry<String, List<String>> e : sent) {
            String tmp = e.getKey() + ":";
            for (String s : e.getValue()) {
                tmp += s + " ";
            }
            headersParams.add(tmp);
        }
        model.addAttribute("lstHeader", headersParams);
        return "exemple";
    }

    // Lier les beans
    @GetMapping("/testbindpath/{id}/{prenom}/{nom}")
    public String testBindObjPath(Personne per, Model model) {
        model.addAttribute("personne", per);
        return "exemple";
    }

    @GetMapping("/testbindparam")
    public String testBindObjParam(Personne per, Model model) {
        model.addAttribute("personne", per);
        return "exemple";
    }

    // Redirection
    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/exemple/hello";
    }

    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/exemple/hello";
    }

    // ModelAttribute
    // sur les paramètres de la fonction
    @GetMapping("/testmodelattrib")
    public String testParamModelAttribute(@ModelAttribute("per1") Personne per, Model model) {
        model.addAttribute("msg", per);
        return "exemple";
    }

    // Sur une méthode
    @ModelAttribute("cpt")
    public int testModelAttribute() {
        return cpt++;
    }

    @ModelAttribute
    public void testModelAttribute2(Model model) {
        model.addAttribute("per1", new Personne("John", "Doe"));
    }

    // Flash attribute
    @GetMapping("/testflashattr")
    public String testFlashAttribute(RedirectAttributes rAtt) {
        rAtt.addAttribute("msgFlash", "Vous venez d'être redirigé");
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/cibleflash")
    public String cibleFlashAttribute(@ModelAttribute("msgFlash") String msgFlash, Model model) {
        model.addAttribute("msg", msgFlash);
        return "exemple";
    }

    // Exception
    @GetMapping("/genioexception")
    void genIoException() throws IOException {
        throw new IOException("IO exception");
    }

    @GetMapping("/gensqlexception")
    void genSqlException() throws SQLException {
        throw new SQLException("Sql exception");
    }

    @ExceptionHandler(IOException.class)
    public String handlerIOException(Exception e, Model model) {
        model.addAttribute("msgEx", e.getMessage());
        model.addAttribute("traceEx", e.getStackTrace());
        return "exception";
    }

    // Cookie
    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse response) {
        Cookie c = new Cookie("testCookie", "valeur_de_test");
        c.setMaxAge(30);
        response.addCookie(c);
        return "redirect:/exemple/readcookie";
    }

    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(value = "testCookie", defaultValue = "default value cookie") String value,
            Model model) {
        model.addAttribute("msg", "Cookie testCookie=" + value);
        return "exemple";
    }

    // Session
    // Méthode 1: Session JavaEE
    @GetMapping("/writesession1")
    public String writeSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("user1", new Personne("John", "Doe"));
        model.addAttribute("msg", "Ajout du utilisateur dans la session 1");
        return "exemple";
    }

    @GetMapping("/readsession1")
    public String readSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        Personne per1 = (Personne) session.getAttribute("user1");
        if (per1 != null) {
            model.addAttribute("msg", per1);
        }
        return "exemple";
    }

    // Methode 2: @SessionAttribute
    @GetMapping("/writesession2")
    public String writeSession2(Model model) {
        Personne p = new Personne("Jane", "Doe");
        model.addAttribute("user2", p);
        model.addAttribute("msg", "Ajout du utilisateur dans la session 2");
        return "exemple";
    }

    @GetMapping("/readsession2")
    public String readSession2(@ModelAttribute("user2") Personne per, Model model) {
        model.addAttribute("msg", per);
        return "exemple";
    }

    @ModelAttribute("user2")
    public Personne initSessionMeth2() {
        return new Personne();
    }

    // Méthode 3: Session
    @GetMapping("/writesession3")
    public String writeSession3(Model model) {
        Personne p = provider.get();
        p.setId(3);
        p.setPrenom("Alan");
        p.setNom("Smithee");
        model.addAttribute("msg", "Ajout du utilisateur dans la session 3");
        return "exemple";
    }

    @GetMapping("/readsession3")
    public String readSession3(Model model) {
        Personne p = provider.get();
        if (p != null) {
            model.addAttribute("msg", p);
        }
        return "exemple";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("user-file") MultipartFile mpf, Model model) {
        String info = mpf.getOriginalFilename() + " " + mpf.getSize();
        model.addAttribute("msg", info);
        try (BufferedOutputStream bow = new BufferedOutputStream(
                new FileOutputStream("c:\\Dawan\\Formations\\" + mpf.getOriginalFilename()))) {
            bow.write(mpf.getBytes());
            bow.flush();
        } catch (IOException e) {

        }
        return "exemple";
    }

    @GetMapping("/exportarticles")
    public void downloadCsv(HttpServletResponse response) {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachement;filename=articles.csv");
        List<Article> lstArt = dao.findAll();
        try {
            PrintWriter pw = response.getWriter();
            pw.println("id;description;prix;date_ajout;emballage");
            for (Article a : lstArt) {
                pw.println(a.getId() + ";" + a.getDescription() + ";" + a.getPrix() + ";" + a.getDateAjout() + ";"
                        + a.getEmballage());
            }
            pw.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @GetMapping("/freemarker/{i}")
    public String testFreemarker(@PathVariable int i, Model model) {
        model.addAttribute("msg", "Hello world");
        model.addAttribute("i", i);
        model.addAttribute("lst", dao.findAll());
        return "freemarker";
    }
    
    @GetMapping("/testmail")
    public String testMail() {
        SimpleMailMessage message=new SimpleMailMessage();
        message.setTo("jd@dawan.com");
        message.setFrom("test@dawa.com");
        message.setSubject("un email de test");
        message.setText("Le contenu du message");
        emailSender.send(message);
        
        return"redirect:/exemple";
    }
}
