package fr.dawan.springmvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import fr.dawan.springmvc.beans.Article;
import fr.dawan.springmvc.beans.BasketLine;
import fr.dawan.springmvc.dao.ArticleDao;

@Controller
@RequestMapping("/articles")
@SessionAttributes("panier")
public class ArticleController {

    @Autowired
    ArticleDao dao;
   // FakeArticleDao dao;

    @GetMapping("")
    public String getAll(Model model) {
        List<Article> articles = dao.findAll();
        model.addAttribute("articles", articles);
        return "store";
    }

    @GetMapping("/basket")
    public String getBasket(@ModelAttribute("panier") List<BasketLine> panier, Model model) {
        model.addAttribute("panier", panier);
        return "basket";
    }

    @GetMapping("/basketadd/{id}")
    public String addBasket(@ModelAttribute("panier") List<BasketLine> panier, @PathVariable int id, Model model) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.incQuantity();
        }
        else {
            Article article = dao.find(id);
            panier.add(new BasketLine(article, 1));
        }
        return "redirect:/articles";
    }

    @GetMapping("/basketremove/{id}")
    public String deleteBasket(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            panier.remove(bl);
        }
        return "redirect:/articles/basket";
    }

    @GetMapping("/basketinc/{id}")
    public String incQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.incQuantity();
        }
        return "redirect:/articles/basket";
    }

    @GetMapping("/basketdec/{id}")
    public String decQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        BasketLine bl=findBasketLine(id, panier);
        if(bl!=null) {
            bl.decQuantity();
            if(bl.getQuantity()<=0) {
                panier.remove(bl);
            }
        }
        return "redirect:/articles/basket";
    }
    
    @ModelAttribute("panier")
    public List<BasketLine> initPanier() {
        return new ArrayList<>();
    }
    
    
    private BasketLine findBasketLine(long id,List<BasketLine> panier) {
        for (BasketLine b : panier) {
            if (b.getArticle().getId() == id) {
               return b;
            }
        }
        return null;
    }
}
