package fr.dawan.springmvc.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.beans.Article;
import fr.dawan.springmvc.beans.User;
import fr.dawan.springmvc.dao.ArticleDao;
import fr.dawan.springmvc.dao.FakeUserDao;
import fr.dawan.springmvc.forms.ArticleForm;
import fr.dawan.springmvc.forms.UserForm;
import fr.dawan.springmvc.forms.UserValidator;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    ArticleDao dao;
    //FakeArticleDao dao;
    @Autowired
    FakeUserDao userDao;
    
    @GetMapping("/articles")
    public String displayArticle(Model model) {
        List<Article> articles=dao.findAll();
        model.addAttribute("articles",articles);
        return "articles";
    }
    
    @GetMapping("/articles/delete/{id}")
    public String deleteArticle(@PathVariable int id) {
        dao.delete(id);
        return "redirect:/admin/articles";
    }
    
    @GetMapping("/articles/add")
    public String addArticle(@ModelAttribute("formarticle") ArticleForm userForm) {
        return "addarticle";
    }
    
    @PostMapping("/articles/add")
    public ModelAndView addArticle(@Valid @ModelAttribute("formarticle") ArticleForm articleForm,BindingResult results) {
        ModelAndView mdv=new ModelAndView();
        if(results.hasErrors()) {
            mdv.setViewName("addarticle");
            mdv.addObject("formarticle",articleForm);
            mdv.addObject("errors",results);
        }
        else {
            Article ar=new Article(articleForm.getDescription(),articleForm.getPrix(),articleForm.getDateAjout(),articleForm.getEmballage());
            dao.saveOrUpdate(ar);
            mdv.setViewName("redirect:/admin/articles");
        }
        return mdv;
    }
    
    @GetMapping("/utilisateurs")
    public String displayUtilistateur(Model model) {
        List<User> users=userDao.findAll();
        model.addAttribute("users",users);
        return "users";
    }
    
    @GetMapping("/utilisateurs/add")
    public String addUser(@ModelAttribute("formuser") UserForm userForm) {
        return "adduser";
    }
    
//    @PostMapping("/utilisateurs/add")
//    public ModelAndView addUser(@Valid @ModelAttribute("formuser") UserForm userForm, BindingResult result) {
//       ModelAndView mdv=new ModelAndView();
//        if(result.hasErrors()) {
//            mdv.setViewName("adduser");
//            mdv.addObject("formuser", userForm);
//            mdv.addObject("errors",result);
//            
//        }else {
//            User u=new User(userForm.getPrenom(),userForm.getNom(),userForm.getEmail(),userForm.getPassword());
//            userDao.saveOrUpdate(u);
//            mdv.setViewName("redirect:/exemple");
//        }
//        return mdv;    
//    }
    
    @PostMapping("/utilisateurs/add")
    public ModelAndView addUser(@ModelAttribute("formuser") UserForm userForm, BindingResult results) {
        new UserValidator().validate(userForm, results);
       ModelAndView mdv=new ModelAndView();
        if(results.hasErrors()) {
            mdv.setViewName("adduser");
            mdv.addObject("formuser", userForm);
            mdv.addObject("errors",results);
            
        }else {
            User u=new User(userForm.getPrenom(),userForm.getNom(),userForm.getEmail(),userForm.getPassword());
            userDao.saveOrUpdate(u);
            mdv.setViewName("redirect:/exemple");
        }
        return mdv;    
    }
}
