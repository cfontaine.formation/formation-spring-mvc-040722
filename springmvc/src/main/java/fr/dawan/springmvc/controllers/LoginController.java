package fr.dawan.springmvc.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.beans.User;
import fr.dawan.springmvc.dao.FakeUserDao;
import fr.dawan.springmvc.forms.LoginForm;

@Controller
@SessionAttributes("isConnected")
public class LoginController {

    @Autowired
    private FakeUserDao dao;

    @GetMapping("/login")
    public String getLogin(@ModelAttribute("formlogin") LoginForm form) {
        return "login";
    }

    @PostMapping("/login")
    public ModelAndView login(@Valid @ModelAttribute("formlogin") LoginForm userLogin, BindingResult results, Model model) {
        ModelAndView mdv=new ModelAndView("redirect:/login");
        if (results.hasErrors()) {
            mdv.addObject("formlogin", userLogin);
            mdv.addObject("errors", results);
            mdv.setViewName("login");
        } else {
            User userDb = dao.findByEmail(userLogin.getEmail());
            if (userDb != null && userDb.getPassword().equals(userLogin.getPassword())) {
                mdv.addObject("isConnected", true);
                mdv.setViewName("redirect:/admin/articles");
            }
        }
        return mdv;
    }

//    @PostMapping("/login")
//    public String login(@RequestParam String email, @RequestParam String password,Model model) {
//        User userDb=dao.findByEmail(email);
//        if(userDb!=null && userDb.getPassword().equals(password)) {
//            model.addAttribute("isConnected", true);
//            return "redirect:/admin/articles";
//        }
//        return "redirect:/login";
//    }

    @GetMapping("/logout")
    public String logout(Model model) {
        model.addAttribute("isConnected", false);
        return "redirect:/exemple";
    }

    @ModelAttribute("isConnected")
    public Boolean initLogin() {
        return false;
    }
}
