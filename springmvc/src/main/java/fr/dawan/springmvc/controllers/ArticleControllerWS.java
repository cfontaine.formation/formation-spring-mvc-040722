package fr.dawan.springmvc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springmvc.beans.Article;
import fr.dawan.springmvc.dao.FakeArticleDao;

@RestController
@RequestMapping("/api/articles")
public class ArticleControllerWS {

    @Autowired
    private FakeArticleDao dao;

    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Article> getAllArticle() {
        return dao.findAll();
    }

    @ResponseStatus(code = HttpStatus.I_AM_A_TEAPOT)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Article getArticleById(@PathVariable int id) {
        return dao.find(id);
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Article addArticle(@RequestBody Article article) {
        dao.saveOrUpdate(article);
        return article;
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable int id) {
        try {
            dao.delete(id);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Article updateArticle(@RequestBody Article article) {
        dao.saveOrUpdate(article);
        return article;
    }

}
