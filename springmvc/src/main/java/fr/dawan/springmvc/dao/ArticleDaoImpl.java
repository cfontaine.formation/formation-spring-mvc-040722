package fr.dawan.springmvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springmvc.beans.Article;

//@Repository
@Transactional
public class ArticleDaoImpl implements ArticleDao {
    
    @PersistenceContext
    EntityManager em;

    @Override
    public void saveOrUpdate(Article a) {
        if(a.getId()==0) {
            em.persist(a);
        }else {
            em.merge(a);
        }
    }

    @Override
    public void delete(Article a) {
      em.remove(a);
    }

    @Override
    public void delete(long id) {
        em.remove(em.find(Article.class, id));
    }

    @Override
    @Transactional(readOnly = true )
    public Article find(long id) {
       return em.find(Article.class, id);
    }

    @Override
    public List<Article> findAll() {
        TypedQuery<Article>req=em.createQuery("SELECT a FROM Article a",Article.class);
        return req.getResultList();
    }

}
