package fr.dawan.springmvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.dawan.springmvc.beans.User;

@Component
public class FakeUserDao {

    private static List<User> users = new ArrayList<>();

    static {
       User u=new User("John","Doe","jd@dawan.com","123456");
       u.setId(1);
       users.add(u);
       u=new User("Jane","Doe","ja@dawan.com","123456");
       u.setId(2);
       users.add(u);
    }

    static int cpt =3;
    
    public void saveOrUpdate(User a) {
        if (a.getId() == 0) {
            users.add(a);
            cpt++;
            a.setId(cpt);
        } else {
            for (int i = 0; i <users.size(); i++) {
                if (users.get(i).getId() == a.getId()) {
                    users.set(i, a);
                }
            }
        }
    }

    public void delete(User a) {
        users.remove(a);
    }

    public void delete(long id) {
        User a = find(id);
        users.remove(a);
    }

    public User find(long id) {
        for (User a : users) {
            if (a.getId() == id) {
                return a;
            }
        }
        return null;
    }

    public List<User> findAll() {
        return users;
    }
    
    public User findByEmail(String email) {
        for(User u :users) {
            if(u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

}


