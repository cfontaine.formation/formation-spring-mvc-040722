package fr.dawan.springmvc.dao;

import java.util.List;

import fr.dawan.springmvc.beans.Article;

public interface ArticleDao {

    void saveOrUpdate(Article a);

    void delete(Article a);

    void delete(long id);

    Article find(long id);

    List<Article> findAll();
}
