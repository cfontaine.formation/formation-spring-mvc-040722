package fr.dawan.springmvc.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.dawan.springmvc.beans.Article;
import fr.dawan.springmvc.enums.Emballage;

@Component
public class FakeArticleDao {

    private static List<Article> articles = new ArrayList<>();

    static {
        Article a = new Article("TV 4K 60 pouce", 600.0, LocalDate.of(2021, 11, 15), Emballage.CARTON);
        a.setId(1);
        articles.add(a);
        a = new Article("Smartphone Android", 350.0, LocalDate.of(2021, 6, 2), Emballage.CARTON);
        a.setId(2);
        articles.add(a);
        a = new Article("Souris Gaming", 30.0, LocalDate.of(2019, 02, 4), Emballage.PLASTIQUE);
        a.setId(3);
        articles.add(a);
        a = new Article("Clavier", 9.0, LocalDate.of(2018, 10, 21), Emballage.SANS);
        a.setId(4);
        articles.add(a);

    }

    static int cpt =4;
    
    public void saveOrUpdate(Article a) {
        if (a.getId() == 0) {
            articles.add(a);
            cpt++;
            a.setId(cpt);
        } else {
            for (int i = 0; i <articles.size(); i++) {
                if (articles.get(i).getId() == a.getId()) {
                    articles.set(i, a);
                }
            }
        }
    }

    public void delete(Article a) {
        articles.remove(a);
    }

    public void delete(long id) {
        Article a = find(id);
        articles.remove(a);
    }

    public Article find(long id) {
        for (Article a : articles) {
            if (a.getId() == id) {
                return a;
            }
        }
        return null;
    }

    public List<Article> findAll() {
        return articles;
    }


}


