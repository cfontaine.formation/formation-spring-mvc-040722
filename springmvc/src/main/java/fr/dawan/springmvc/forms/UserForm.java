package fr.dawan.springmvc.forms;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserForm {
    
    @NotNull
    @Size(max=50)
    private String prenom;
    
    @NotNull
    @Size(min = 2, max=50)
    private String nom;
    
    @NotNull
    @Email
    private String email;
    
    @NotNull
    @Size(min=6,max=40)
    private String password;
    
    @NotNull
    @Size(min=6,max=40)
    private String confirmPassword;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    
}
