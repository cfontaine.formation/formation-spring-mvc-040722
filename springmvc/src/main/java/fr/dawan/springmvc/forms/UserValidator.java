package fr.dawan.springmvc.forms;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz==UserForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserForm formUser=(UserForm)target;
        
        ValidationUtils.rejectIfEmpty(errors, "prenom","user.premon.Empty" , "le prénom ne doit pas être vide");
        if(formUser.getPassword()==null || formUser.getConfirmPassword()==null ||
                !formUser.getPassword().equals(formUser.getConfirmPassword())) {
            formUser.setPassword("");
            formUser.setConfirmPassword("");
            errors.rejectValue("confirmPassword","user.confirmPassWord.NotEquals", "password & confirmPassword are not equals ");
        }

    }

}
