package fr.dawan.springmvc.beans;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.dawan.springmvc.enums.Emballage;

@Entity
@Table(name="articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false,length = 120)
    private String description;

    @Column(nullable = false)
    private double prix;

    @Column(name = "date_ajout",nullable = false)
    private LocalDate dateAjout;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Emballage emballage;

    public Article() {

    }

    public Article(String description, double prix, Emballage emballage) {
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
        dateAjout = LocalDate.now();
    }

    public Article(String description, double prix, LocalDate dateAjout, Emballage emballage) {
        this.description = description;
        this.prix = prix;
        this.dateAjout = dateAjout;
        this.emballage = emballage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(LocalDate dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Emballage getEmballage() {
        return emballage;
    }

    public void setEmballage(Emballage emballage) {
        this.emballage = emballage;
    }

}


