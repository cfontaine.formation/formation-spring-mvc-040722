package fr.dawan.springmvc.beans;

public class BasketLine {
    
    private Article article;
    
    private int quantity;

    public BasketLine() {

    }

    public BasketLine(Article article, int quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incQuantity() {
        quantity++;
    }
    
    public void decQuantity() {
        quantity--;
    }
}
