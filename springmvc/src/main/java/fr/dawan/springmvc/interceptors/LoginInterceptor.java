package fr.dawan.springmvc.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        HttpSession session=request.getSession();
        Boolean isConnected=(Boolean) session.getAttribute("isConnected");
        if(isConnected!=null && isConnected==true) {
            return true;
        }
        else {
            request.getRequestDispatcher("/login").forward(request, response);
            return false;
        }
    }

//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//            ModelAndView modelAndView) throws Exception {
//        System.out.println("Méthode postHandle");
//        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
//            throws Exception {
//        System.out.println("Méthode afterCompletion");
//        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
//    }

}
