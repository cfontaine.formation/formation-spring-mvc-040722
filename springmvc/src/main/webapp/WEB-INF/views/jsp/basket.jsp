<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="header.jsp">
    <c:param name="Panier" value="Login" />
</c:import>

<h1 class="text-center my-5">Panier</h1>
    <c:choose>
    <c:when test="${ not empty panier}"> 
    <table class="table">
    
        <c:forEach items="${panier}" var="basketline">
            <tr>
                <td><c:out value="${basketline.article.description}" /></td>
                <td>
                <a class="btn btn-sm btn-secondary" href="<c:url value='/basketdec/${basketline.article.id}' context='/springmvc/articles'/>">-</a>
                <c:out value="${basketline.quantity}" />
                 <a class="btn btn-sm btn-secondary" href="<c:url value='/basketinc/${basketline.article.id}' context='/springmvc/articles'/>">+</a>
                <c:set var="total" value="${total= total+ basketline.article.prix * basketline.quantity} "/>
                </td>
                <td><p><c:out value="${basketline.article.prix * basketline.quantity}" /></p></td>
                <td><a class="btn btn-sm btn-danger" href="<c:url value='/basketremove/${basketline.article.id}' context='/springmvc/articles'/>">X</a></td>
            </tr>
        </c:forEach>
        <tr><td></td><td class="fw-bolder">Total</td><td class="fw-bolder"><c:out value="${total}"/></td><td></td></tr>
    </table>
    </c:when>
    <c:otherwise>
       <h3 class="text-center my-5 text-secondary">Vide</h3>    
    </c:otherwise>
    </c:choose>
    <c:import url="footer.jsp" />
