<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Exemple JSTL</title>
</head>
<body>
<h1>Exemple JSTL</h1>

<h2>c:out</h2>
<div><c:out value="${msg}"/></div>

<h2>c:set</h2>
<c:set var="i" value="10"/>
<div><c:out value="${i}"/></div>
<c:remove var="i"/> 
<div><c:out value="${i}" default="variable supprimée"/></div>

<h2>c:if</h2>
<c:if test="${v1>10}">
    <div><c:out value="${v1}"/> est supérieur à 10</div>
</c:if>

<c:if test="${empty v1}">
    <div>v1 n'existe pas</div>
</c:if>

<h2>c:choose</h2>
<c:choose>
    <c:when test="${empty v1}">
         <div>v1 n'existe pas</div>   
    </c:when>
    <c:when test="${v1>10}">
         <div><c:out value="${v1}"/> est supérieur à 10</div>
    </c:when>
    <c:otherwise>
        <div>v1 &lt; ou = à 10 </div>
    </c:otherwise>
</c:choose>

<h2>c:foreach parcours d'une collection</h2>
<ul>
<c:forEach items="${produits}" var="pr">
    <li><c:out value="${pr}"/></li>
</c:forEach>
</ul>
<ul>
<c:forEach items="${produits}" var="pr" begin="1" end="2" varStatus="vs">
    <li><c:out value="${vs.index} ${pr}"/></li>
</c:forEach>
</ul>

<h2>c:foreach équivalent d'un for</h2>
<c:forEach begin="2" end="3" var="j">
    <c:out value="${j}"/>
</c:forEach>

<h2>c:forToken</h2>
<c:forTokens items="azer;tyui;opqsd" delims=";" var="str">
    <c:out value="${str}"/><br/>
</c:forTokens>

<h2>c:url</h2>
<c:url value="/testpath/42"  context="/springmvc/exemple" var="urlpath" />
<a href="${urlpath}">exemple test PathVariable</a>
<a href='<c:url value="/testpath/44"  context="/springmvc/exemple" />'>exemple test PathVariable</a>
</body>
</html>