<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp">
    <c:param name="titre" value="Admnistration des articles"/>
</c:import>

<h1>Admnistration des articles</h1>
<a class="btn btn-sm btn-outline-secondary mt-3" href='<c:url value="/admin/articles/add" context="/springmvc"/>'/>Ajouter articles</a>
<table class="table">
    <thead>
        <tr>
        <th>Id</th>
        <th>Description</th>
        <th>Prix</th>
        <th>Date Ajout</th>
        <th>Emballage</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
     <c:forEach items="${articles}" var ="ar">
     <tr>
        <td><c:out value="${ar.id}"/></td>
        <td><c:out value="${ar.description}"/></td>
        <td><c:out value="${ar.prix}"/></td>
        <td><c:out value="${ar.dateAjout}"/></td>
        <td><c:out value="${ar.emballage}"/></td>      
        <c:url value='/admin/articles/delete/${ar.id}' context="/springmvc" var="urlDelete"/>
        <td><a class="btn btn-danger" href="${urlDelete}">Supprimer</a></td>
     </tr>
     </c:forEach>
    </tbody>
</table>

<c:import url="footer.jsp"/>