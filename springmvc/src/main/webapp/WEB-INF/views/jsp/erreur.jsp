<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="page d'erreur"/>
</c:import>
<div class="display-1 text-primary text-center my-5"><c:out value="${codeErreur}"/></div>
<div class="h2 text-center my-5"><c:out value="${msgErreur}"/></div>
<c:import url="footer.jsp"/>