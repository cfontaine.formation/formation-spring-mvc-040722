<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Ajout utilisateur"/>
</c:import>
<h1> Ajouter article</h1>

<c:url value="/admin/articles/add" context="/springmvc" var="urlform"/>
<form:form method="post" action="${urlform}" modelAttribute="formarticle">
    <div class="mb-2">
    <form:label path="description" class="form-label">Description</form:label>
    <form:input path="description" placeholder="description" type="text" class="form-control"/>
    <form:errors path="description" class="text-danger small"/>
    </div>

    <div class="mb-2">
    <form:label path="prix" class="form-label">Prix</form:label>
    <form:input path="prix" placeholder="prix" type="number" class="form-control" value="0" step="0.01"/>
    <form:errors path="prix" class="text-danger small"/>
    </div>

    <div class="mb-2">
    <form:label path="dateAjout" class="form-label">Date d'ajout</form:label>
    <form:input path="dateAjout" placeholder="description" type="date" class="form-control"/>
    <form:errors path="dateAjout" class="text-danger small"/>
    </div>

	<div class="mb-2">
	<form:label class="form-label" path="emballage">Emballage </form:label>
	<form:select class="form-select" path="emballage">
	  <form:option value="CARTON">Carton</form:option>
	  <form:option value="PLASTIQUE">Plastique</form:option>
	  <form:option value="SANS">Sans</form:option>
	</form:select>
	</div>
    <input class="btn btn-primary" type="submit" value="Ajouter"/>

</form:form>
<c:import url="footer.jsp"/>