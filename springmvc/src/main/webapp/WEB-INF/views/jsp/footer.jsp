<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <footer>
    <hr/>
    <p class="text-center small">Formation Spring MVC - Dawan - Juillet 2022</p>     
    </footer>
</main>
<script src='<c:url value="/webjars/bootstrap/5.1.3/js/bootstrap.bundle.min.js" context="/springmvc"/>'></script>
</body>
</html>