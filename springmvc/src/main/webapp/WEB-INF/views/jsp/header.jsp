<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:url value="/webjars/bootstrap/5.1.3/css/bootstrap.min.css" context="/springmvc" var="urlBootstrap"/>
<link rel="stylesheet" href="${urlBootstrap}">
<title><c:out value='${empty param.titre?"Spring MVC":param.titre}'/></title>
</head>
<body>

<nav class="navbar navbar-expand-lg <spring:theme code='navbar'/> <spring:theme code='navbar-bg'/> mb-5 sticky-top">

    <a class="navbar-brand" href="/springmvc/exemple"><spring:message code="brand" text="Formation Spring MVC"/> </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container-fluid">
     <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
       
        <li class="nav-item">
          <a class="nav-link"  aria-current="page" href="<c:url value='/exemple' context='/springmvc/exemple"'/>"><spring:message code="exemple"/></a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link"  aria-current="page" href="<c:url value='/' context='/springmvc/presentation"'/>"><spring:message code="presentation"/></a>
        </li>
       

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLang" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Administrateur
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLang">
                <li><a class="dropdown-item" href='<c:url value="/admin/articles" context="/springmvc"/>'><spring:message code="articles"/></a></li>
            </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link"  aria-current="page" href="<c:url value='/' context='/springmvc/articles"'/>"><spring:message code="articles"/></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLang" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <spring:message code="langue" text="Langue"/>
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLang">
            <li><a class="dropdown-item" href="?lang=fr">Français</a></li>
            <li><a class="dropdown-item" href="?lang=en">Anglais</a></li>
            <li><a class="dropdown-item" href="?lang=es">Espagnol</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuTheme" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <spring:message code="theme" text="Thème"/>
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuTheme">
            <li><a class="dropdown-item" href="?theme=default">Sombre</a></li>
            <li><a class="dropdown-item" href="?theme=red">Rouge</a></li>
            <li><a class="dropdown-item" href="?theme=blue">Bleu</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuTheme" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <spring:message code="vue" text="Vue"/>
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuTheme">
            <li><a class="dropdown-item" href='<c:url value="/exemple/testjstl" context="/springmvc"/>'>Jsp</a></li>
<!--             <li><a class="dropdown-item" href="/exemple/freemarker">Freemarker</a></li> -->
          </ul>
        </li>
      </ul>
        <a class="text-decoration-none  mx-5 text-light"  aria-current="page" href="<c:url value='/' context='/springmvc/articles/basket"'/>"><spring:message code="basket"/></a>
        <c:choose>
        <c:when test='${empty sessionScope.isConnected ||  ! sessionScope.isConnected}'>
            <a class="btn btn-success btn-sm" href="<c:url value='/login' context='/springmvc'/>" ><spring:message code="login"/></a>
        </c:when>
        <c:otherwise>
            <a class="btn btn-secondary btn-sm" href="<c:url value='/logout' context='/springmvc'/>" ><spring:message code="logout"/></a>
        </c:otherwise>
        </c:choose>
     
     </div>

     
</div>

</nav>
<main class="container-fluid">