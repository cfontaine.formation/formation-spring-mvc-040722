<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Exemple"/>
</c:import>

<h1>Exemple</h1>

<div class="text-secondary fs-3 col-md-6 my-2">Compteur <c:out value="${ cpt}"/>(@ModelAttribute)</div>

<c:if test="${! empty msg }">
<div class="alert alert-primary mt-5 col-md-6">
    <c:out value="${msg}"/>
</div>
</c:if>

<div class="row">
<div class="col-md-6">

<h4>Controleur</h4>
<ul>
    <li><a class="my-2" href='<c:url value="/exemple/hello" context="/springmvc"/>'>Helloworld</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testmodel" context="/springmvc"/>'>Model</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testmodelandview" context="/springmvc"/>'>ModelAndView</a></li>
</ul>
<h4>Request Mapping</h4>
<ul>
    <li><a class="my-2" href='<c:url value="/exemple/testparams?id=12" context="/springmvc"/>'>url avec paramètre id=12</a></li>
    <li><a class="my-2 text-decoration-line-through" href='<c:url value="/exemple/testparams?id=3" context="/springmvc"/>'>url avec paramètre id=3 => erreur</a></li>
    <li><a class="my-2 text-decoration-line-through" href='<c:url value="/exemple/testparams" context="/springmvc"/>'>url sans paramètre => erreur</a></li>
</ul>
<h4>Path Variable</h4>
<ul>
    <li><a class="my-2" href='<c:url value="/exemple/testpath/2" context="/springmvc"/>'>url /exemple/testpath/2</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testpathmulti/42/action/ajouter" context="/springmvc"/>'>url /exemple/testpathmulti/42/action/ajouter</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testpathamb/42" context="/springmvc" />'>url avec /exemple/testpathamb/42</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testpathamb/smithee" context="/springmvc" />'>url avec /exemple/testpathamb/Smithee</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testpathoption" context="/springmvc" />'>url avec /exemple/testpathoption</a></li>
    <li><a class="my-2" href='<c:url value="/exemple/testpathoption/123" context="/springmvc" />'>url avec /exemple/testpathoption/123</a></li>
</ul>
<h4>RequestParam</h4>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/testparam?id=123" context="/springmvc" />'>test paramètre get id=23</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testparamdefault?id=56" context="/springmvc" />'>test paramètre par défaut get id=56</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testparamdefault" context="/springmvc" />'>test paramètre par défaut</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testparamconv?id=5" context="/springmvc" />'>test conversion paramètre int </a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testconvdate?date=06-07-2022" context="/springmvc" />'>test de conversion date=2022-07-06</a></li>
</ul>

<h4>RequestHeader</h4>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/testheader" context="/springmvc"/>'>request header: user-agent</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testallheader" context="/springmvc"/>'>all header</a></li>
</ul>
<c:if test="${ !empty lstHeader}">
    <ul>
        <c:forEach items="${lstHeader}" var="elm">
            <li class="my-2"><c:out value="${elm}" /></li>
        </c:forEach>
    </ul>
</c:if>
<h4>Lier les beans</h4>
<c:if test="${!empty personne}">
    <div class ="alert alert-primary"><c:out value="${personne.prenom} ${personne.nom}"/></div>
</c:if>
<ul>
    <li><a class="my-2" href='<c:url value="/exemple/testbindpath/1/john/doe" context="/springmvc"/>'>Path :John Doe</a>
    <li><a class="my-2" href='<c:url value="/exemple/testbindparam?id=4&prenom=alan&nom=smithee" context="/springmvc"/>'>Param : Alan  Smithee</a>
</ul>


<h4>Redirection et FlashAttribute</h4>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/testredirect" context="/springmvc" />'>Test redirection(redirect) hello world</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testforward" context="/springmvc" />'>Test redirection(forward) hello world</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/testflashattr" context="/springmvc" />'>Test flash attributes</a></li>
</ul>
<h4>Erreurs et Exceptions</h4>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/fourofour" context="/springmvc" />'>Erreur 404</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/genioexception" context="/springmvc" />'>Générer une IOException</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/gensqlexception" context="/springmvc" />'>Générer une SQLException</a></li>
</ul>
</div>
<div class="col-6 border-start">
<h4>Cookie</h4>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/readcookie" context="/springmvc" />'>Lire la valeur dans le cookie testCookie</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/writecookie" context="/springmvc" />'>Ecrire le cookie testCookie </a></li>
</ul>
<h4>Session</h4>
<h5>Avec HttpSession:</h5>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/writesession1" context="/springmvc" />'>Ecrire un utilisateur user1(John Doe) dans la session</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/readsession1" context="/springmvc" />'>Lire un utilisateur user1 dans la session</a></li>
</ul>
<h5>Avec @SessionAttribute, @ModelAttribute:</h5>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/writesession2" context="/springmvc" />'>Ecrire un utilisateur user2(Jane Doe) dans la session</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/readsession2" context="/springmvc" />'>Lire un utilisateur user2 dans la session</a></li>
</ul>
<h5>Avec un bean avec une portée session :</h5>
 <ul>
  <li><a class="my-2" href='<c:url value="/exemple/writesession3" context="/springmvc" />'>Ecrire un utilisateur user3(Allan Smithee) dans la session</a></li>
  <li><a class="my-2" href='<c:url value="/exemple/readsession3" context="/springmvc" />'>Lire un utilisateur user3 dans la session</a></li>
</ul>

<h4> Upload Fichier</h4>
<form method="post" action='<c:url value="/exemple/upload" context="/springmvc"/>' enctype="multipart/form-data">
    <label for='user-file' class="form-label">Fichier</label>
    <input type="file" id="user-file" name="user-file" class="form-control"/>
    <input type="submit" value="Télécharger" class="btn btn-primary"/>
</form>

</div>


</div>

<c:import url="footer.jsp"/>