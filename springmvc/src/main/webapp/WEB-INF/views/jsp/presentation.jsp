<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp">
    <c:param name="titre" value="présentation"/>
</c:import>
<h1>Présentation</h1>

<c:if test="${!empty prenom && !empty nom }">
<div class="alert alert-primary">Bonjour, <c:out value="${prenom} ${nom}"/></div>
</c:if>

<div class="my-4">
    <a href='<c:url value="/presentation/path/john/doe" context="/springmvc"/>'>Présentation path : John Doe</a>
</div>
<c:url value="/presentation/param" context="/springmvc" var="urlpar">
    <c:param name="prenom" value="Jane"/>
    <c:param name="nom" value="Doe"/>
</c:url>

<div class="my-4">
    <a href="${urlpar}">Présentation param: Jane Doe</a>
</div>

<form method="post" action='<c:url value="/presentation/param" context="/springmvc"/>' >
 <div class="mb-3">
    <label for="prenom" class="form-label">Prénom</label>
    <input class="form-control" type="text" name="prenom" id="prenom" placeholder="prénom"/>
 </div> 
 <div class="mb-3">  
    <label for="nom" class="form-label">Nom</label>
    <input class="form-control" type="text" name="nom" id="nom" placeholder="nom"/>
 </div>
<input class="btn btn-primary mt-3 col-md-3" type="submit"value="Présentation"/>
</form>

<c:import url="footer.jsp"/>