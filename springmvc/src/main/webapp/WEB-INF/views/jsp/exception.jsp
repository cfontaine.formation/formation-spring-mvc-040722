<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="exception"/>
</c:import>

<div class="display-4 text-center mb-3" ><c:out value="${msgEx}"/></div>
<c:forEach items="${traceEx}" var="trace">
    <div class="text-danger col-md-6 offset-md-3"><c:out value="${trace}"/></div>
</c:forEach>

<c:import url="footer.jsp"/>