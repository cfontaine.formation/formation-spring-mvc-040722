<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:import url="header.jsp">
    <c:param name="titre" value="login"/>
</c:import>
<h1>Authentification</h1>
<c:url value="/login" context="/springmvc" var="urllogin"/>
<form:form method="post" action="${urllogin}"  modelAttribute="formlogin">

    <div class="mb-3">
    <form:label path="email" class="form-label">Email</form:label>
    <form:input type="email" path="email" placeholder="email" class="form-control"/>
    <form:errors path="email" class="text-danger small"/>
    </div>
    <div class="mb-3">
    <form:label path="password" class="form-label">Password</form:label>
    <form:password path="password" placeholder="password" class="form-control"/>
    <form:errors path="password" class="text-danger small"/>
    </div>
    <input type="submit" class="btn btn-primary" value="Connexion"/>
</form:form>

<c:import url="footer.jsp"/>