<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <c:import url="header.jsp">
        <c:param name="titre" value="Articles"/>
    </c:import>
    <div class ="row">
    <c:forEach items="${articles}" var="article">
    <div class="card text-center col-md-1 g-4" style="width: 18rem;">
    <h5 class="card-title"><c:out value="${article.description}"/></h5>
    <p class="card-text"><small class="text-muted"><c:out value = "${article.dateAjout}" /></small></p>
    <p><c:out value = "${article.prix} €"/></p>
    <a href="<c:url value='/basketadd/${article.id}' context='/springmvc/articles'/>" class="card-link my-2">Ajouter au panier</a>
    </div>
    </c:forEach>
</div>
<c:import url="footer.jsp"/>