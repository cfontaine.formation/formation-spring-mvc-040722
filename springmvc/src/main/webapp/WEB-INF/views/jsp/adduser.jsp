<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Ajout utilisateur"/>
</c:import>

<h1> Ajouter utilisateur</h1>
<c:url value="/admin/utilisateurs/add" context="/springmvc" var="urlform"/>
<form:form method="post" action="${urlform}" modelAttribute="formuser">
    <div class="mb-2">
    <form:label path="prenom" class="form-label">Prénom</form:label>
    <form:input path="prenom" placeholder="prénom" type="text" class="form-control"/>
    <form:errors path="prenom" class="text-danger small"/>
    </div>
    <div class="mb-2">
    <form:label path="nom" class="form-label">Nom</form:label>
    <form:input path="nom" placeholder="nom" type="text" class="form-control"/>
    <form:errors path="nom"  class="text-danger small"/>
    </div>
    <div class="mb-2">
    <form:label path="email" class="form-label">Email</form:label>
    <form:input path="email" placeholder="email" type="email" class="form-control"/>
    <form:errors path="email"  class="text-danger small"/>
        </div>
    <div class="mb-2">
    <form:label path="password" class="form-label">Password</form:label>
    <form:input path="password" placeholder="password" type="password" class="form-control"/>
    <form:errors path="password"  class="text-danger small"/>
        </div>
    <div class="mb-2">
    <form:label path="confirmPassword" class="form-label">Confirme password</form:label>
    <form:input path="confirmPassword" placeholder="password" type="password" class="form-control"/>
    <form:errors path="confirmPassword"  class="text-danger small"/>
    </div>
    <input class="btn btn-primary" type="submit" value="Ajouter"/>
</form:form>

<c:import url="footer.jsp"/>